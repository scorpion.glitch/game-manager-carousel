package com.scorpionglitch.gmcarousel.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scorpionglitch.gmcarousel.models.DiscCarousel;
import com.scorpionglitch.gmcarousel.models.DiscCarouselManager;

@RestController
@RequestMapping("/api/carousel")
public class CarouselController {
	@Autowired
    private DiscCarouselManager discCarouselManager;

    @GetMapping("/")
    public ResponseEntity<Set<String>> getDiscCarouselUniqueIDs() {
        return ResponseEntity.ok(discCarouselManager.getUniqueIDs());
    }

    @GetMapping("/{discCarouselID}")
    public ResponseEntity<DiscCarousel> getDiscCarousel(@PathVariable String discCarouselID) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        DiscCarousel discCarousel = discCarouselManager.getDiscCarousel(discCarouselID);
        if (discCarousel != null) {
            return new ResponseEntity<DiscCarousel>(discCarousel, headers, HttpStatus.OK);
        }
        return new ResponseEntity<DiscCarousel>(headers, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{discCarouselID}/eject/{position}")
    public ResponseEntity<String> ejectDisc(@PathVariable String discCarouselID, @PathVariable int position) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        try {
            discCarouselManager.ejectDisc(discCarouselID, position);
        } catch (NullPointerException npe) {
            return new ResponseEntity<String>(headers, HttpStatus.NOT_FOUND);
        } catch (IndexOutOfBoundsException ioobe) {
            return new ResponseEntity<String>(headers, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>(headers, HttpStatus.OK);
    }
}
