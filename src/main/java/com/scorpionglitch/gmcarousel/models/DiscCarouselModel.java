package com.scorpionglitch.gmcarousel.models;

import org.usb4java.Device;

public enum DiscCarouselModel {
	DACAL_DC_300(0x04B4, 0x5A9B, new DiscCarouselFactory() {
		@Override
		public DiscCarousel getInstance(Device device) {
			return new DacalDC300(device);
		}
	});
	
	public final int VENDOR_ID;
	public final int PRODUCT_ID;
	
	private final DiscCarouselFactory DISC_CAROUSEL_FACTORY;
	
	private DiscCarouselModel(int vendorID, int productID, DiscCarouselFactory discCarouselFactory) {
		this.VENDOR_ID = vendorID;
		this.PRODUCT_ID = productID;
		this.DISC_CAROUSEL_FACTORY = discCarouselFactory;
	}
	
	public DiscCarousel getInstance(Device device) {
		return DISC_CAROUSEL_FACTORY.getInstance(device);
	}
}
