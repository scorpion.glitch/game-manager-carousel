package com.scorpionglitch.gmcarousel.models;

public interface DiscCarousel {
	String getUniqueID();
	void retrieveDisc(int position);
	boolean isConnected();
}
