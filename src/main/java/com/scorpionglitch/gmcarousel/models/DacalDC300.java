package com.scorpionglitch.gmcarousel.models;

import java.nio.ByteBuffer;

import org.usb4java.Device;
import org.usb4java.DeviceHandle;
import org.usb4java.LibUsb;

public class DacalDC300 implements DiscCarousel {
	private Device device;
	private DeviceHandle deviceHandle;
	private ByteBuffer byteBuffer;
	
	private Integer deviceID;
	private String uniqueID;
	
	/* TODO confirm buffer size for Dacal DC 300*/
	private final int BUFFER_SIZE = 514;

	public DacalDC300(Device device) {
		this.device = device;
		this.byteBuffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
		this.deviceHandle = new DeviceHandle();
	}
	
	private int getDeviceID() {
        if (this.deviceID == null) {
            LibUsb.open(device, deviceHandle);
            if (LibUsb.kernelDriverActive(deviceHandle, (byte)0x0) == 1) {
                if (LibUsb.detachKernelDriver(deviceHandle, (byte) 0x0) != LibUsb.SUCCESS) {
                    // TODO handle error
                    LibUsb.close(deviceHandle);
                    return 0;
                }
            }
            if (LibUsb.claimInterface(deviceHandle, (byte) 0x0) == LibUsb.SUCCESS) {
                LibUsb.getStringDescriptor(deviceHandle, (byte) 0x0A, (short) 0x409, byteBuffer);
                deviceID = (byteBuffer.get(2) << 8 | byteBuffer.get(4) << 4 | byteBuffer.get(6));
            }
            LibUsb.attachKernelDriver(deviceHandle, (byte) 0x0);
            LibUsb.close(deviceHandle);
        }
        return deviceID;
    }

    @Override
    public String getUniqueID() {
        if (uniqueID == null) {
            uniqueID = String.format("%04X%04X%08X", 0x04B4, 0x5A9B, getDeviceID());
        }
        return uniqueID;
    }

    @Override
    public void retrieveDisc(int position) {
        LibUsb.open(device, deviceHandle);
        if (LibUsb.kernelDriverActive(deviceHandle, (byte)0x0) == 1) {
            if (LibUsb.detachKernelDriver(deviceHandle, (byte) 0x0) != LibUsb.SUCCESS) {
                // TODO handle error
                LibUsb.close(deviceHandle);
                return;
            }
        }
        if (LibUsb.claimInterface(deviceHandle, (byte) 0x0) == LibUsb.SUCCESS) {
            LibUsb.getStringDescriptor(deviceHandle, (byte) 0x0E, (short) 0x409, byteBuffer);
            LibUsb.getStringDescriptor(deviceHandle, (byte) 0x0C, (short) 0x409, byteBuffer);
            LibUsb.getStringDescriptor(deviceHandle, (byte) position, (short) 0x409, byteBuffer);
        }
        LibUsb.attachKernelDriver(deviceHandle, (byte) 0x0);
        LibUsb.close(deviceHandle);
    }
    
    @Override
    public boolean isConnected() {
    	int result = LibUsb.open(device, deviceHandle);
    	if (result < 0) {
    		return false;
    	}
        LibUsb.close(deviceHandle);
    	return true;
    }
}
