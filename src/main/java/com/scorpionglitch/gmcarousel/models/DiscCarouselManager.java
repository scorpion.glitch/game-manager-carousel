package com.scorpionglitch.gmcarousel.models;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.usb4java.Context;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;
import org.usb4java.LibUsbException;

@Component
public class DiscCarouselManager {
	private Context context;

    private Map<String, DiscCarousel> discCarousels;

    @Autowired
    public DiscCarouselManager() {
    	super();
        System.out.println("intansiating DiscCarouselManager");
    	this.context = new Context();
        LibUsb.init(context);
        discCarousels = new HashMap<String, DiscCarousel>();
        
        new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						removeDisconnectedCarousels();
						discoverNewCarousels();
						Thread.sleep(TimeUnit.SECONDS.toMillis(30));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
        }).start();
    }
    
    private void removeDisconnectedCarousels() {
    	String[] keys = new String[discCarousels.keySet().size()];
    	for (String key : discCarousels.keySet().toArray(keys)) {
    		DiscCarousel carousel = discCarousels.get(key);
    		if (!carousel.isConnected()) {
    			discCarousels.remove(key);
    		}
    	}
    }
    
    private void discoverNewCarousels() {
    	DeviceList deviceList = new DeviceList();
    	int result = LibUsb.getDeviceList(context, deviceList);
    	if (result < 0)
    		throw new LibUsbException("Unable to get device list", result);
    	try {
    		deviceList.forEach(device -> {
    			DeviceDescriptor descriptor = new DeviceDescriptor();
    			if (LibUsb.getDeviceDescriptor(device, descriptor) != LibUsb.SUCCESS) 
    				throw new LibUsbException("Unable to read device descriptor", result);
    			if (descriptor.idVendor() == 0x04B4 && descriptor.idProduct() == 0x5A9B) 
    			{
    				DiscCarousel discCarousel = DiscCarouselModel.DACAL_DC_300.getInstance(device);
    				discCarousels.put(discCarousel.getUniqueID(), discCarousel);
    			}
    		});
    	} finally {
    		LibUsb.freeDeviceList(deviceList, false);
    	}
    }
    
    /*
    private void registerCallbacks() {
    	System.out.println("Registering callbacks");
        for (DiscCarouselModel discCarouselModel : DiscCarouselModel.values()) {
            registerCallbacks(discCarouselModel);
        }
    }

    private void registerCallbacks(DiscCarouselModel discCarouselModel) {
    	System.out.println("Registering callbacks for " + discCarouselModel.VENDOR_ID + ":" + discCarouselModel.PRODUCT_ID );
        LibUsb.hotplugRegisterCallback(context, LibUsb.HOTPLUG_EVENT_DEVICE_ARRIVED, LibUsb.HOTPLUG_NO_FLAGS,
                discCarouselModel.VENDOR_ID, discCarouselModel.PRODUCT_ID,
                LibUsb.HOTPLUG_MATCH_ANY,
                new HotplugCallback() {
                    @Override
                    public int processEvent(Context context, Device device, int i, Object o) {
                        System.out.println("connected");
                        DiscCarousel discCarousel = discCarouselModel.getInstance(device);
                        discCarousels.put(discCarousel.getUniqueID(), discCarousel);
                        return LibUsb.SUCCESS;
                    }
                }, null, null);
        LibUsb.hotplugRegisterCallback(context, LibUsb.HOTPLUG_EVENT_DEVICE_LEFT, LibUsb.HOTPLUG_NO_FLAGS,
                discCarouselModel.VENDOR_ID, discCarouselModel.PRODUCT_ID,
                LibUsb.HOTPLUG_MATCH_ANY,
                new HotplugCallback() {
                    @Override
                    public int processEvent(Context context, Device device, int event, Object userData) {
                        System.out.println("disconnected");
                        DiscCarousel discCarousel = deviceDiscCarouselMap.remove(device);
                        discCarousels.remove(discCarousel.getUniqueID());
                        return LibUsb.SUCCESS;
                    }
                }, null, null);
    }
    // */

    public Set<String> getUniqueIDs() {
        return discCarousels.keySet();
    }

    public DiscCarousel getDiscCarousel(String discCarouselID) {
        return discCarousels.get(discCarouselID);
    }

    public void ejectDisc(String discCarouselID, int position) {
        discCarousels.get(discCarouselID).retrieveDisc(position);
    }
}
