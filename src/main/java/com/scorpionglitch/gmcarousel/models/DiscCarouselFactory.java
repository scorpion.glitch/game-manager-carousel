package com.scorpionglitch.gmcarousel.models;

import org.usb4java.Device;

public interface DiscCarouselFactory {
	public DiscCarousel getInstance(Device device);
}
