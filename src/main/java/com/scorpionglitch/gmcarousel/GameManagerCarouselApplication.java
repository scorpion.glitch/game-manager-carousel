package com.scorpionglitch.gmcarousel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class GameManagerCarouselApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameManagerCarouselApplication.class, args);
	}

}
